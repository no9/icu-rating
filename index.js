module.exports = calculate;

function calculate (rating, games, kFactor) {
    var Ro = rating;
    var K = kFactor;
    var We = 0;
    var internalWe = 0;
    // We assume we have won all the games
    var W = games.length;
    var opponentSum = 0;
    var resultSum = 0;
    for (var gamecounter = 0; gamecounter < games.length; gamecounter++) {
        // keep a running total of the results not including draws
        if(games[gamecounter].result % 1 === 0) {
            resultSum += games[gamecounter].result;
        }
        //decrement where there are draws and losses
        if(games[gamecounter].result < 1) {
            //Ensure it's a positive number
            var x = Math.abs(games[gamecounter].result);
            // Convert it to a negative number
            x *= -1;
            // Apply it to the total
            W += x;
        }
        var Rd = rating - games[gamecounter].rating;
        opponentSum += games[gamecounter].rating;
        // wg = 1 / (1 + 10^(Rd/400))
        internalWe += 1 / (1 + Math.pow(10, (Rd/400)));
    }
        We = +(Math.round(games.length - internalWe + "e+2")  + "e-2");
        //Rn = Ro + K(W - We)
        if(W >= 0) {
            Rn = Ro + (K * (W - We));
        } else {
            // Have to abs the result and change the sign for a loss
            Rn = Ro - (K * (Math.abs(W) - We));
        }
        var retval = {};
        retval.newRating = Math.round(Rn);
        retval.noOfGames = games.length;
        retval.score = W;
        retval.expectedScore = We;        
        retval.performance = (opponentSum + (400 * resultSum)) / games.length ; 
    return retval;
}
