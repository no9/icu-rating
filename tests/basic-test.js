var test = require('tap').test;
var calc = require('../index');

test('won game', function (t) {
    t.plan(5);
    var games = [];
    var game = {
        rating : 1600,
        result : 1
    };

    games.push(game);
    var nr = calc(1600, games, 24);
    t.equal(nr.newRating, 1612);
    t.equal(nr.noOfGames, 1);
    t.equal(nr.score, 1);
    t.equal(nr.expectedScore, 0.5);
    t.equal(nr.performance, 2000);

});

test('lost game', function(t) {
    t.plan(5);
    var games = [];
    var game = {
        rating : 1600,
        result : -1
    };

    games.push(game);
    var nr = calc(1600, games, 24);
    t.equal(nr.newRating, 1588);
    t.equal(nr.noOfGames, 1);
    t.equal(nr.score, 0);
    t.equal(nr.expectedScore, 0.5);
    t.equal(nr.performance, 1200);
});

test('drawn game', function(t) {
    t.plan(5);
    var games = [];
    var game = {
        rating : 1600,
        result : 0.5
    };

    games.push(game);
    var nr = calc(1600, games, 24);
    t.equal(nr.newRating, 1600);
    t.equal(nr.noOfGames, 1);
    t.equal(nr.score, 0.5);
    t.equal(nr.expectedScore, 0.5);
    t.equal(nr.performance, 1600);
});

test('two lost games', function (t) {
    t.plan(5);
    var games = [];
    var game = {
        rating : 1600,
        result : -1
    };

    games.push(game);
    games.push(game);
    var nr = calc(1600, games, 24);
    t.equal(nr.newRating, 1576);
    t.equal(nr.noOfGames, 2);
    t.equal(nr.score, 0);
    t.equal(nr.expectedScore, 1);
    t.equal(nr.performance, 1200);
});

test('two drawn games', function (t) {
    t.plan(5);
    var games = [];
    var game = {
        rating : 1600,
        result : 0.5
    };

    games.push(game);
    games.push(game);
    var nr = calc(1600, games, 24);
    t.equal(nr.newRating, 1600);
    t.equal(nr.noOfGames, 2);
    t.equal(nr.score, 1);
    t.equal(nr.expectedScore, 1);
    t.equal(nr.performance, 1600);
});

test('two won games', function (t) {
    t.plan(5);
    var games = [];
    var game = {
        rating : 1600,
        result : 1
    };

    games.push(game);
    games.push(game);
    var nr = calc(1600, games, 24);
    t.equal(nr.newRating, 1624);
    t.equal(nr.noOfGames, 2);
    t.equal(nr.score, 2);
    t.equal(nr.expectedScore, 1);
    t.equal(nr.performance, 2000);
});

test('two won games different ratings', function (t) {
    t.plan(5);
    var games = [];
    var game = {
        rating : 1600,
        result : 1
    };
   var game1 = {
        rating : 1700,
        result : 1
    };


    games.push(game);
    games.push(game1);

    var nr = calc(1600, games, 24);
    t.equal(nr.newRating, 1627);
    t.equal(nr.noOfGames, 2);
    t.equal(nr.score, 2);
    t.equal(nr.expectedScore, 0.86);
    t.equal(nr.performance, 2050);
});

test('two won games different lower ratings', function (t) {
    t.plan(5);
    var games = [];
    var game = {
        rating : 1600,
        result : 1
    };
   var game1 = {
        rating : 1500,
        result : 1
    };


    games.push(game);
    games.push(game1);

    var nr = calc(1600, games, 24);
    t.equal(nr.newRating, 1621);
    t.equal(nr.noOfGames, 2);
    t.equal(nr.score, 2);
    t.equal(nr.expectedScore, 1.14);
    t.equal(nr.performance, 1950);
});



test('three games', function (t) {
    t.plan(5);
    var games = [];
    var game1 = {
        rating : 1600,
        result : 1
    };
    var game2 = {
        rating : 1600,
        result : 0.5
    };
    var game3 = {
        rating : 1600,
        result : -1
    };
    games.push(game1);
    games.push(game2);
    games.push(game3);
    
    var nr = calc(1600, games, 24);
    t.equal(nr.newRating, 1600);
    t.equal(nr.noOfGames, 3);
    t.equal(nr.score, 1.5);
    t.equal(nr.expectedScore, 1.5);
    t.equal(nr.performance, 1600);
});
