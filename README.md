# icu-rating

A js library based on the ICU rating calculation - https://www.icu.ie/articles/3
Generates a new rating based on a current rating an array of games and a kFactor.
Games are an array of game objects consisting of the rating of an opponent and the result of the game.

### usage 

```
var rating = require('icu-rating');

var games = [];
    var game = {
        rating : 1600,
        result : 1
    };
    games.push(game);

    var newRating = rating(1600, games, 24)
    console.log(JSON.stringify(newRating))
```
Will print
```
{"newRating":1612,"noOfGames":1,"score":1,"expectedScore":0.5,"performance":2000}
```
